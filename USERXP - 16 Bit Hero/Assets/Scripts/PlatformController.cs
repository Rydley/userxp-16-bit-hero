﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{

    public GameObject target;
    public Vector3 offset;

    public float speed;
    public Vector3 direction;
    public float distance;
    public Vector3 startPos;

    private Rigidbody2D rb;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        target = null;
        startPos = transform.position;
    }

    void OnTriggerStay2D(Collider2D col)
    {
        target = col.gameObject;
        offset = target.transform.position - transform.position;
    }
    void OnTriggerExit2D(Collider2D col)
    {
        target = null;
    }

    void Update()
    {
        if (transform.position.x > startPos.x + distance)
            direction = -direction;

        if (transform.position.x < startPos.x)
            direction = Vector3.right;

        transform.Translate(direction * speed * Time.deltaTime);
    }
    void LateUpdate()
    {
        if (target != null)
        {
            target.transform.position = transform.position + offset;
        }
    }
}
