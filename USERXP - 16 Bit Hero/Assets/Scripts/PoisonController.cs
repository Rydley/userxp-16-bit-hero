﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonController : MonoBehaviour {

    public Transform target;

    public float speed;

    private Rigidbody2D rb;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        Vector2 direction = (Vector2)target.position - rb.position;
        direction.Normalize();
    }
	
	// Update is called once per frame
	void Update () {
        rb.velocity = transform.up * speed;
    }
}
