﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour {

    public int levelIndex;
    public string levelName;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            SceneManager.LoadScene(levelIndex, LoadSceneMode.Single);

            //SceneManager.LoadScene(levelName, LoadSceneMode.Single);
        }
    }
}
